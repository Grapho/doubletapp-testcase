import os

from datetime import datetime

from src.general import Application
from src.general import check2desu
from src.general import generate_uuid
from src.general import General

db = Application.db


class Pets_DB(db.Model):
    __tablename__ = 'pets'
    id = db.Column(db.String, primary_key=True, default=generate_uuid)
    name = db.Column(db.String, unique=False)
    age = db.Column(db.Integer)
    has_photos = db.Column(db.Boolean)
    pet_type_id = db.Column(db.Integer)
    created_at = db.Column(db.String)

    @staticmethod
    @check2desu
    def add(name, age, pet_type_id, has_photos=False):
        pet = Pets_DB(
            name=name,
            age=age,
            pet_type_id=pet_type_id,
            has_photos=has_photos,
            created_at=datetime.now()
        )
        db.session.add(pet)
        db.session.commit()

        return pet

    @check2desu
    def delete(self):
        for photo in Photos_DB.query.filter_by(pet_id=self.id).all():
            db.session.delete(photo)
        db.session.delete(self)
        db.session.commit()


class Photos_DB(db.Model):
    __tablename__ = 'photos'
    id = db.Column(db.String, primary_key=True, default=generate_uuid)
    pet_id = db.Column(db.Integer)
    url = db.Column(db.String)

    @staticmethod
    @check2desu
    def add(pet_id, file):
        photo = Photos_DB(
            pet_id=pet_id,
            url='',
        )

        pet = Pets_DB.query.filter_by(id=pet_id).first()
        pet.has_photos = True

        db.session.add_all([photo, pet])
        db.session.commit()

        filename = '.'.join([photo.id, file.filename.split('.')[-1]])
        file.save(os.path.join(General.uploads_path, filename))
        photo.url = os.path.join(General.url_folder, filename)
        db.session.commit()

        return photo


class PetTypes_DB(db.Model):
    __tablename__ = 'pet_types'
    id = db.Column(db.String, primary_key=True, default=generate_uuid)
    name = db.Column(db.String)

    @staticmethod
    @check2desu
    def add(name):
        pet_type = PetTypes_DB(
            name=name,
        )
        db.session.add(pet_type)
        db.session.commit()

        return pet_type
