import os
import sys
import uuid
import logging
import tempfile
import traceback

from flask import Flask
from logging.handlers import RotatingFileHandler
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy

from termcolor import cprint

responses = {
    '200': '200 - OK\n',
    '400': '400 - Bad request\n',
    '401': '401 - Unauthorized\n',
}


def check2desu(foo):
    def wrapper(*args, **kwargs):
        resp = Response()
        try:
            tmp = foo(*args, **kwargs)
            resp.success = True
            resp.result = tmp
        except Exception as exc:
            resp.success = False
            resp.message = exc.args[0]
        return resp
    return wrapper


def check_auth_key(headers):
    auth_key = headers.get('X-API-KEY')
    if auth_key == Application.app.config['API_KEY']:
        return True
    else:
        return responses['401']


def generate_uuid():
    return str(uuid.uuid4())


class General:
    db_name = 'sqlite:///database.db'
    windows_log_path = "C:\\Users\\{current_user}\\AppData\\"
    linux_log_path = '/var/log/'
    static_folder = '/static/'
    upload_folder = f'server{static_folder}uploads/'
    url_folder = f'{static_folder}uploads/'
    server_url = 'https://grapho.pythonanywhere.com'
    basedir = os.path.abspath(os.path.dirname(__file__))
    uploads_path = os.sep.join(
        basedir.split(os.sep)[:-2] + [upload_folder])


class Application:
    if not os.path.exists(General.uploads_path):
        os.makedirs(General.uploads_path)
    app = Flask(__name__, template_folder='../template')
    app.config['API_KEY'] = 'f93412b5-b380-4420-b65f-adc694195102'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = General.db_name

    Base = declarative_base()

    db = SQLAlchemy(app)


class ProjectBIO:
    name = 'doubleTapp'
    version = '1.0.0'
    description = 'Тестовое задание для трудоустройства в doubleTapp'


class Routes:
    base = '/'
    pets = '/pets'
    photo = '/pets/<id>/photo'


class Messages:
    separator = '====== [server started now] ======'


class Warnings:
    wrng = '[System Warning]'


class Errors:
    err = '[System Error] '
    can_not_create_logger = err + 'Не могу создать логгер'
    wrong_params = err + 'Некорректно переданы аргументы запуска'
    smth_went_wrong = err + 'Что-то пошло не так.'


class MessageTypes:
    info = 'info'
    warning = 'warning'
    critical = 'critical'


class Writer(object):
    """
    Вспомогательный класс для взаимодействия с пользователем.
    Пишет сообщения в консоль и лог-файл.
    """
    def __new__(cls):
        """
        Обеспечивает работу класса в режиме синглтона.
        """
        if not hasattr(cls, 'instance'):
            cls.instance = super(Writer, cls).__new__(cls)
            cls.logger = Logger().get_logger()
        return cls.instance

    @classmethod
    def my_print(cls, msg, msg_type=''):
        """
        Метод, который принимает на вход сообщение,
            после чего пишет его в лог файл и выводит в консоль
        :param msg: Сообщение, которое будет записано в лог-файл
                    и выведено в консоль.
        :param msg_type: Тип сообщения, влияет на форматирование
                         выводимого в консоль сообщения
        """
        if msg_type == MessageTypes.info:
            cprint(msg, color='green')
            cls.logger.info('%s', msg)
        elif msg_type == MessageTypes.warning:
            cprint(msg, color='yellow')
            cls.logger.warning('%s', msg)
        elif msg_type == MessageTypes.critical:
            cprint(msg, color='red')
            cls.logger.critical('%s', msg)
        else:
            cprint(msg)
            cls.logger.info('%s', msg)


class Response():
    """
    Вспомогательный класс для обеспечения удобного взаимодействия между
        функциями. Своего рода API, но на уровне разработчика.
    :param success: булевая переменная.
                    True - если все хорошо.
                    False - если произошла ошибка.
    :param result: Результат взаимодействия.
        Используется, если должны передаваться какие-то данные.
    :param message: Сопутствующее сообщение к результату выполнения.
        О возникшей ошибке будет написано в этой переменной.
    """

    def __init__(self):
        self.success = False
        self.result = None
        self.message = str()


class Logger:
    def __init__(self):
        self.log_filename = ProjectBIO.name + ".log"
        tmp = self.__create_logger(__name__)
        if not tmp.success:
            raise Exception(
                Errors.can_not_create_logger)
        self.logger = tmp.result
        sys.excepthook = self.log_uncaught_exceptions

    @check2desu
    def __create_logger(self, name):
        """
        Создание логгера
        :param name:
        :return:
        """
        hndlr = self.get_handler()
        lg = logging.getLogger(name)
        lg.setLevel(logging.INFO)
        lg.addHandler(hndlr)
        lg.info(Messages.separator)
        return lg

    def get_logger(self):
        return self.logger

    def get_log_format(self):
        """
        Get метод для формата лога
        :return: format
        """
        return logging.Formatter(
            u"%(levelname)-8s - %(name)-22s" +
            "[LINE:%(lineno)5d] - %(asctime)s - %(message)s")

    def get_handler(self):
        """
        Метод для формирования handler для логирования
        :return: handler
        """
        hndlr = RotatingFileHandler(
            self.get_log_file(),
            maxBytes=5 * 1024 * 1024,
            backupCount=2,
            delay=0)
        hndlr.setFormatter(self.get_log_format())
        hndlr.setLevel(logging.INFO)

        return hndlr

    def get_log_dir(self, system):
        """
        Метод возвращает директорию для логов в зависимости от ОС
        :param system: ОС
        :return: директория для логов
        """
        # определение директории для логов
        log_dir = General.linux_log_path
        if not ('linux' in system or 'darwin' in system):
            log_dir = General.windows_log_path.format(
                current_user=os.getlogin())

        # создание директории для логов, если её нет
        if not os.path.exists(log_dir):
            try:
                os.makedirs(log_dir)
            except PermissionError:
                print("Ошибка создания каталога {log_dir}".format(
                    log_dir=log_dir))
                return tempfile.gettempdir()

        return log_dir

    def get_log_file(self):
        """
        Метод для создания logger
        :return: log_file
        """
        log_path = self.get_log_dir(sys.platform)

        # проверка существования каталога и прав записи
        if os.access(log_path, os.W_OK):
            log_file = os.path.join(log_path, self.log_filename)
        else:
            filepath = os.path.join(tempfile.gettempdir(), self.log_filename)
            log_file = filepath
            cprint(f"Лог-файл будет создан во временной директории: {filepath}",
                   color='yellow')

        return log_file

    def log_uncaught_exceptions(
            self, exception_type, exception_msg, traceback_msg):
        """
        Метод для логирования необработанных исключений и завершения программы
        :param exception_type: Тип вызванного исключения
        :param exception_msg: Исключение (текст)
        :param traceback_msg: Ошибки построчно
        :return: -
        """
        self.logger.critical(
            "Некорректное завершение программы. Необработанное исключение "
            "{error_type}:\n  {error_msg}: \n{traceback_msg}".
            format(error_type=exception_type.__name__,
                   error_msg=exception_msg,
                   traceback_msg="".join(traceback.format_tb(traceback_msg))))
        sys.exit()
