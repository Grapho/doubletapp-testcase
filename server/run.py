import os
import json
import argparse

from termcolor import cprint

from flask import render_template
from flask import send_from_directory
from flask import request

from src.general import Application
from src.general import Errors
from src.general import General
from src.general import MessageTypes
from src.general import ProjectBIO
from src.general import Routes
from src.general import Writer
from src.general import check2desu
from src.general import check_auth_key
from src.general import responses

from src.models import Pets_DB
from src.models import PetTypes_DB
from src.models import Photos_DB


app = Application.app
db = Application.db


@app.route(Routes.base)
def main_view():
    Writer().my_print(
        msg=f'{Routes.base} - login success',
        msg_type=MessageTypes.info)

    datatable_lables = {
        'id': 'ID',
        'name': 'Имя',
        'age': 'Возраст',
        'photos': 'Фотографии',
        'pet_type': 'Тип животного',
        'created_at': 'Дата создания'
    }

    pet_types = {t.id: t.name for t in PetTypes_DB().query.all()}
    photos = dict()
    for item in Photos_DB().query.all():
        try:
            photos[item.pet_id]
        except KeyError:
            photos[item.pet_id] = list()
        photos[item.pet_id].append(item.url)
    datatable = Pets_DB.query.all()

    return render_template(
        'base.html',
        page_title=ProjectBIO.name,
        datatable=datatable,
        labels=datatable_lables,
        pet_types=pet_types,
        photos=photos,
    )


@app.route(Routes.pets, methods=["POST"])
def add_pets():
    check = check_auth_key(request.headers)
    if type(check) is str:
        return check
    try:
        name: str = request.form['name']
        age: int = request.form['age']
        pet_type_name: int = request.form['type']
    except KeyError:
        return responses['400']

    try:
        files = request.files.getlist('photos[]')
    except KeyError:
        return responses['400']

    pet_type = PetTypes_DB.query.filter_by(name=pet_type_name).first()
    if not pet_type:
        pet_type = PetTypes_DB.add(name=pet_type_name).result

    pet = Pets_DB.add(
        name=name,
        age=age,
        pet_type_id=pet_type.id,
    ).result

    for file in files:
        Photos_DB.add(
            file=file,
            pet_id=pet.id,
        )

    return json.dumps({
        "id": pet.id,
        "name": pet.name,
        "age": pet.age,
        "type": PetTypes_DB.query.filter_by(id=pet.pet_type_id).first().name,
        "photos": [
            p.url for p in Photos_DB.query.filter_by(pet_id=pet.id).all()
        ],
        "created_at": pet.created_at
    })


@app.route(Routes.pets, methods=["GET"])
def get_pets():
    check = check_auth_key(request.headers)
    if type(check) is str:
        return check
    try:
        limit: int = int(request.form['limit'])
    except KeyError:
        limit: int = 20

    try:
        offset: int = int(request.form['offset'] or 0)
    except KeyError:
        offset: int = 0

    try:
        has_photos: str = request.form['has_photos']
    except KeyError:
        has_photos: int = None

    if has_photos == 'true':
        pets = Pets_DB.query.filter_by(
            has_photos=True
        )
    elif has_photos == 'false':
        pets = Pets_DB.query.filter(
            Pets_DB.has_photos is False
        )
    else:
        pets = Pets_DB.query
    pets = pets.all()[offset:limit+offset]

    result = {
        "count": len(pets),
        "items": [{
            'id': p.id,
            'name': p.name,
            'age': p.age,
            'type': PetTypes_DB.query.filter_by(id=p.pet_type_id).first().name,
            'photos': [{
                'id': t.id,
                'url': ''.join([
                    request.host_url,
                    t.url[1:]
                ]),
            } for t in Photos_DB.query.filter_by(pet_id=p.id).all()],
            'created_at': p.created_at,
        } for p in pets]
    }

    return json.dumps(result)


@app.route(Routes.pets, methods=["DELETE"])
def delete_pets():
    check = check_auth_key(request.headers)
    if type(check) is str:
        return check

    try:
        ids = request.form.getlist('ids[]')
    except KeyError:
        return responses['400']

    for item in ids:
        try:
            Pets_DB.query.filter_by(id=item).first().delete()
        except AttributeError:
            return responses['400']

    return responses['200']


@app.route(Routes.photo, methods=["POST"])
def add_photo(id):
    check = check_auth_key(request.headers)
    if type(check) is str:
        return check

    try:
        files = request.files.getlist('photos[]')
    except KeyError:
        return responses['400']

    for file in files:
        Photos_DB.add(
            file=file,
            pet_id=id,
        )

    return responses['200']


@app.route('/static/<path>/<path:filename>')
def media(path, filename):
    return send_from_directory(
        ''.join([os.path.abspath(
            os.path.dirname(__file__)
        ) + General.static_folder, path]),
        filename)


@check2desu
def get_db_content(has_photos):
    if has_photos:
        pets = Pets_DB.query.filter_by(
            has_photos=True)
    else:
        pets = Pets_DB.query

    return json.dumps({
        "pets": [{
            'id': p.id,
            'name': p.name,
            'age': p.age,
            'type': PetTypes_DB.query.filter_by(id=p.pet_type_id).first().name,
            'photos': [{
                'url': ''.join([
                    General.server_url,
                    t.url,
                ]),
            } for t in Photos_DB.query.filter_by(pet_id=p.id).all()],
            'created_at': p.created_at,
        } for p in pets.all()]
    })


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='doubleTapp testcase')
    parser.add_argument(
        '--get-db-content',
        action='store_true',
        dest='get_db_content',
        help='return a json like database content')
    parser.add_argument(
        '--has-photos',
        action='store_true',
        dest='has_photos',
        default=False,
        help='return a json like database content only with any photos')
    args = parser.parse_args()
    if args.get_db_content:
        getter = get_db_content(args.has_photos)
        if not getter.success:
            Writer().my_print(
                msg=Errors.smth_went_wrong,
                msg_type=MessageTypes.critical)
        else:
            Writer().my_print(
                msg=getter.result)
    elif args.has_photos and not args.get_db_content:
        Writer().my_print(
            msg=Errors.wrong_params,
            msg_type=MessageTypes.critical)
    else:
        db.create_all()
        app.run(host='0.0.0.0', port=9090, debug=False)
